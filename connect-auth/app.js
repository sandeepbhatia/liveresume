// Import
var level = process.argv.indexOf('-d') === -1 ? 6 : 7;
var logger = new (require('caterpillar').Logger)({ level: level });
var filter = new (require('caterpillar-filter').Filter)();
var human = new (require('caterpillar-human').Human)();

// Pipe logger output to filter, then filter output to stdout
logger.pipe(filter).pipe(human).pipe(process.stdout);
var request = require('request');
var htmlparser = require("htmlparser2");
var connect = require('connect');
var auth = require('../lib/index')
var url = require('url')
var fs = require('fs');
var datag = require('../routes/datagatherer.js').DataGatherer;
var utilities = require('../routes/utilities.js').Utility;
var inference = require('../routes/keywordinference.js').KeywordInference;
var POSTagger = require('../routes/postagger.js').POSTagger;
var KMeansCluster = require('../routes/kmeansclustering.js').KMeansCluster;
var WordBagComputer = require('../routes/computewordbags.js').WordBagComputer;
var Point = require('../routes/wordbagpoint.js').Point;
var Lexer = require('../routes/lexer.js').Lexer;
var https = require('https');
var http = require('http');
var nodeUtil = require("util");
var DOWNLOAD_DIR = '../downloads/';
var crypto = require('crypto');
var md5 = crypto.createHash('md5');
var pdftotext = require('pdftotextjs');
var utility = new utilities(logger);
var keywordInference = new inference(utility);

/*
PDF will not work for that install and work 
with poppler-utils.

Ubuntu/Debian sudo apt-get install poppler-utils.
MacOSX sudo port install poppler or brew install xpdf.
Windows download and install Xpdf. */

var dataSink = new datag('localhost', 27017, function (obj) {
    //TODO : utility.trace('Mongo DB connection created');
});
var OAuth = require('oauth').OAuth;
var lexer = new Lexer(function () {
    //TODO : utility.trace('lexer created');
});

var kMeansCluster = new KMeansCluster(utility);

var posTagger = new POSTagger();
var serverName = 'localhost';
var getSharedSecretForUserFunction = function (user, callback) {
    var result;
    if (user == 'foo')
        result = 'bar';
    callback(null, result);
};

var validatePasswordFunction = function (username, password, successCallback, failureCallback) {
    if (username === 'foo' && password === "bar") {
        successCallback();
    } else {
        failureCallback();
    }
};

// N.B. TO USE Any of the OAuth or RPX strategies you will need to provide
// a copy of the example_keys_file (named keys_file) 
try {
    var example_keys = require('./keys_file');
    for (var key in example_keys) {
        global[key] = example_keys[key];
    }
}
catch (e) {
    //TODO : console.log('Unable to locate the keys_file.js file.  Please copy and ammend the keys_file.js as appropriate');
}

// Setup the 'template' pages (don't use sync calls generally, but meh.)
var authenticatedContent = fs.readFileSync(__dirname + "/public/authenticated.html", "utf8");
var unAuthenticatedContent = fs.readFileSync(__dirname + "/public/unauthenticated.html", "utf8");
var recommendationHtml = fs.readFileSync(__dirname + "/../webapp/recommendation.html", "utf8");
var displayboardHtml = fs.readFileSync(__dirname + "/../webapp/resumelets.html", "utf8");
var mydisplayboardHtml = fs.readFileSync(__dirname + "/../webapp/myresumelets.html", "utf8");
var rawviewHtml = fs.readFileSync(__dirname + "/../webapp/views/index.html", "utf8");
var resumeletsJs = fs.readFileSync(__dirname + "/../webapp/appjs/resumeletsController.js", "utf8");
var myresumeletsJs = fs.readFileSync(__dirname + "/../webapp/appjs/myresumeletsController.js", "utf8");
var clustersJs = fs.readFileSync(__dirname + "/../webapp/appjs/clusterController.js", "utf8");
var clustersHtml = fs.readFileSync(__dirname + "/../webapp/clusters.html", "utf8");
// There appear to be Scurrilous ;) rumours abounding that connect-auth
var contentJs = fs.readFileSync(__dirname + "/../js/content.js", "utf8");
var recommendationJs = fs.readFileSync(__dirname + "/../webapp/appjs/recommendationController.js", "utf8");
// doesn't 'work with connect' as it does not act like an 'onion skin'
// to address this I'm showing how one might extend the *PRIMITIVES* 
// provided by connect-auth to simplify a middleware layer. 

// This middleware detects login requests (in this case requests with a query param of ?login_with=xxx where xxx is a known strategy)
var example_auth_middleware = function () {
    return function (req, res, next) {
        var urlp = url.parse(req.originalUrl, true)
        if (urlp.query.login_with) {
            req.authenticate([urlp.query.login_with], function (error, authenticated) {
                if (error) {
                    // Something has gone awry, behave as you wish.
                    res.end();
                }
                else {
                    if (authenticated === undefined) {
                        // The authentication strategy requires some more browser interaction, suggest you do nothing here!
                    }
                    else {
                        // We've either failed to authenticate, or succeeded (req.isAuthenticated() will confirm, as will the value of the received argument)
                        next();
                    }
                }
            });
        }
        else {
            next();
        }
    }
};

process.argv.forEach(function (val, index, array) {
    if (val == 'trace') {
        utility.enableTracing();
    }
});

if (!String.prototype.trim) {
    //code for trim
    String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, ''); };
}

process.on('uncaughtException', function (err) {
    logger.log('emergency', 'Caught exception: ' + err.stack);
});

var app = connect();


app.use(connect.static(__dirname + '/public'))
   .use(connect.cookieParser('my secret here'))
   .use(connect.session())
   .use(connect.bodyParser())
   .use(auth({
       strategies: [auth.Anonymous()
              , auth.Basic({ validatePassword: validatePasswordFunction })
              , auth.Bitbucket({ consumerKey: bitbucketConsumerKey, consumerSecret: bitbucketConsumerSecret, callback: bitbucketCallbackAddress })
              , auth.Digest({ getSharedSecretForUser: getSharedSecretForUserFunction })
              , auth.Http({ validatePassword: validatePasswordFunction, getSharedSecretForUser: getSharedSecretForUserFunction })
              , auth.Never()
              , auth.Twitter({ consumerKey: twitterConsumerKey, consumerSecret: twitterConsumerSecret })
              , auth.Skyrock({ consumerKey: skyrockConsumerKey, consumerSecret: skyrockConsumerSecret, callback: skyrockCallbackAddress })
              , auth.Facebook({ appId: fbId, appSecret: fbSecret, scope: "email", callback: fbCallbackAddress, utilities: utility })
              , auth.Github({ appId: ghId, appSecret: ghSecret, callback: ghCallbackAddress })
              , auth.Yahoo({ consumerKey: yahooConsumerKey, consumerSecret: yahooConsumerSecret, callback: yahooCallbackAddress })
              , auth.Google({ consumerKey: googleConsumerKey, consumerSecret: googleConsumerSecret, scope: "", callback: googleCallbackAddress })
              , auth.Google2({ appId: google2Id, appSecret: google2Secret, callback: google2CallbackAddress, requestEmailPermission: true })
              , auth.Foursquare({ appId: foursquareId, appSecret: foursquareSecret, callback: foursquareCallbackAddress })
              , auth.Janrain({ apiKey: janrainApiKey, appDomain: janrainAppDomain, callback: janrainCallbackUrl })
              , auth.Getglue({ appId: getGlueId, appSecret: getGlueSecret, callback: getGlueCallbackAddress })
              , auth.Openid({ callback: openIdCallback })
              , auth.Yammer({ consumerKey: yammerConsumerKey, yammerSecret: yammerConsumerSecret, callback: yammerCallback })
              , auth.Linkedin({ consumerKey: linkedinConsumerKey, consumerSecret: linkedinConsumerSecret, callback: linkedinCallback, datasink: dataSink, utilities: utility })],
       trace: true,
       logoutHandler: require('../lib/events').redirectOnLogout("/")
   }))
   .use(example_auth_middleware())

   .use('/logout', function (req, res, params) {
       req.session.loggedId = undefined;
       req.logout(); // Using the 'event' model to do a redirect on logout.
   })

app.use('/getuser', function (req, res, next) {
    logger.log('info', "the logged in user id is : " + req.session.loggedId);
    res.end(req.session.loggedId);
});

app.use('/displayboard', function (req, res, params) {
    var id = -1;
    try {
        id = utility.push('displayboard');
        var userId = utility.getUser(req);
        if (userId == null || userId == undefined) {
            res.writeHead(400, { 'Content-Type': 'text/html' });
            res.end('Security error while accessing the resource');
        }
        else {
            res.end(displayboardHtml);
        }
    }
    finally {
        utility.pop('displayboard', id);
    }
});

app.use('/mydash', function (req, res, params) {
    var userId = utility.getUser(req);
    if (userId == null || userId == undefined) {
        res.writeHead(400, { 'Content-Type': 'text/html' });
        res.end('Security error while accessing the resource');
    }
    else {
        res.writeHead(200, { 'Content-Type': 'text/html' })
        res.end(mydisplayboardHtml);
    }
});

app.use('/webapp/appjs/recommendationController.js', function (req, res, params) {
    //TODO : utility.trace(recommendationJs);
    res.end(recommendationJs);
});

app.use('/webapp/appjs/myresumeletsController.js', function (req, res, params) {
    res.writeHead(200, { 'Content-Type': 'script' })
    res.end(myresumeletsJs);
});

app.use('/webapp/appjs/resumeletsController.js', function (req, res, params) {
    res.end(resumeletsJs);
});

app.use('/dataview', function (req, res, params) {
    res.end(rawviewHtml);
});

app.use('/content.js', function (req, res) {
    res.end(contentJs);
});

app.use('/recommendation.html', function (req, res, params) {
    //TODO : utility.trace(__dirname);
    res.end(recommendationHtml);
});

app.use('/clusters.html', function (req, res, params) {
    res.writeHead(200, { 'Content-Type': 'text/html' })
    //TODO : utility.trace(__dirname);
    res.end(clustersHtml);
});

app.use('/webapp/appjs/clusterController.js', function (req, res, params) {
    //TODO : utility.trace(__dirname);
    res.end(clustersJs);
});

app.use('/liveresume', function (req, res, params) {
    var cookies = {};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        cookies[parts[0].trim()] = (parts[1] || '').trim();
    });

    //TODO : utility.trace(req.session.loggedId);
    //TODO : utility.trace('the live resume server is functional, health check complete');
    res.end('/* The Breathing Resume Portal, redefining how resumes evolve */');
});

app.use('/getrecos', function (req, res, params) {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var text = query.txt;
    if (text != undefined && text != null) {
        text = text.toLowerCase();
    }
    //TODO : utility.trace(text);
    downloadHTTP(text, function (rawData) {
        keywordInference.scrubHtml(rawData, lexer, posTagger, function (error, sentences) {
            if (error == null) {
                //TODO : utility.trace('the analyze text will be now called');
                keywordInference.analyzeText(sentences, function (error, chiSquare) {
                    if (error == null) {
                        //TODO : utility.trace(chiSquare);
                        dataSink.findAll(function (error, skillArtifactCollection) {
                            res.write(JSON.stringify(skillArtifactCollection));
                            res.end();
                        }, chiSquare, true);

                    }
                });
            };
        });
    });

});

// Function to download file using HTTP.get
var downloadHTTPS = function (file_url, callback) {
    var options = {
        host: url.parse(file_url).host,
        port: 443,
        path: url.parse(file_url).pathname
    };

    var htmlData = '';
    https.get(options, function (res) {
        res.on('data', function (data) {
            htmlData += data;
        }).on('end', function () {
            callback(htmlData);
        });
    });
};


function IsPdf(url) {
    logger.log('info', url);
    if (url.lastIndexOf('pdf') != -1) {
        return true;
    }

    return false;
}

var PDFHandler = function () {
    function _onPDFBinDataReady(data) {
        logger.log(JSON.stringify(data));
    }

    function _onPDFBinDataError() {
    }
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
};

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
}

// Function to download file using HTTP.get
var downloadHTTP = function (file_url, callback) {

    var fileName = null;
    var fileDisk = null;
    var filePath = null;

    if (IsPdf(file_url)) {
        fileName = guid();
        filePath = DOWNLOAD_DIR + fileName + '.pdf';
        fileDisk = fs.createWriteStream(filePath);
    }

    var options = {
        host: url.parse(file_url).host,
        port: 80,
        path: url.parse(file_url).pathname,
        method: 'GET'
    };

    var htmlData = '';
    var videourl = '';
    var videotype = '';
    var parser = new htmlparser.Parser({
        onopentag: function (name, attribs) {
            if (name === "meta")
            {
                if (attribs.property == "og:video") {
                    logger.log('found video tag' + attribs.content);
                    videourl = attribs.content;
                }

                //property="og:video:type" content="application/x-shockwave-flash">
                if (attribs.property == "og:video:type") {
                    logger.log('found video type tag' + attribs.content);
                    videotype = attribs.content;
                }
            }
        }
    });
    
   
    http.get(options, function (res) {
        res.on('data', function (data) {
            htmlData += data;
            parser.write(data);
            if (fileDisk != null) {
                fileDisk.write(data);
            }
        }).on('end', function () {
            if (fileDisk != null) {
                fileDisk.end();
                logger.log('info', fileDisk + ' is downloaded to ' + DOWNLOAD_DIR);
                logger.log('info', filePath);
                parser.end();
                callback(filePath, true, videourl, videotype);

            }
            else {
                if (htmlData == '' || htmlData == null || htmlData == undefined) {
                    //unify these here this is going to be unified with get 

                    request(file_url, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            parser.write(body);
                            callback(body, false, videourl, videotype);
                        }

                        parser.end();
                    })
                }
               
            }
        });
    });
};

function getUrlHash(data) {
    //we will get unique 16 byte hash for each url article supplied
    //TODO : scrub any common intricacies on the same url
    var md5 = crypto.createHash('md5');
    md5.update(data);
    var value = md5.digest('hex');
    //TODO : utility.trace('hex value being displayed' + value);
    return value;

}

function MakeUserAssociation(req, urlhash, annotateText, chiSquare) {
    dataSink.SaveUserAssociation(req, { hash: urlhash, annotate: annotateText }, function (saved) {
        //TODO : utility.trace("the user association was created");
        //TODO : utility.trace(JSON.stringify(saved));
    });
}

function DoArticleInsertion(req, res, query, rawData, urlhash, annotateText, ispdf, title, twitter, videourl, videotype) {
    var link = [rawData];
    var article = dataSink.findArticleByHash(urlhash, function (error, artifact) {
        if (artifact == null) {
            keywordInference.scrubHtml(rawData, lexer, posTagger, ispdf, function (error, sentences) {
                if (error == null) {
                    //TODO : utility.trace('the analyze text will be now called');
                    keywordInference.analyzeText(sentences, function (error, chiSquare) {
                        if (error == null && !(twitter == '1' && chiSquare.length == 0)) {
                            dataSink.SaveSkillArtifact(
                            req,
                            {
                                url: query.url,
                                /* data: query.text, 
                                sentences: JSON.stringify(sentences), */
                                img: query.imgsrc,
                                chi: chiSquare,
                                hash: urlhash,
                                modified: 1,
                                evaluateScore: -1,
                                title: title,
                                video: videourl,
                                videotype: videotype
                            },
                        function (error, docs) {
                            if (error == null) {
                                //TODO : utility.trace(docs);
                                MakeUserAssociation(req, urlhash, annotateText, chiSquare);
                                res.end('data has been accepted');
                            }
                        });
                        }
                    });
                };
            });
        }
        else {
            MakeUserAssociation(req, urlhash, annotateText);
        }
        //TODO : utility.trace("all inserts done");
    });
}

var GetPdfText = function (locationPdf) {
    var pdf = null;
    var data = null;
    logger.log('info', 'trying to get the pdf text');
    pdf = new pdftotext(locationPdf);
    var data = pdf.getTextSync();
    logger.log('info', data);
    return data;
}

app.use('/insert', function (req, res, params) {
    var userId = utility.getUser(req);
    if (userId == null) {
        res.writeHead(400, { 'Content-Type': 'text/html' });
        res.end;
    }
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var cleanedData = '';
    //TODO : utility.trace("annotation recieved is :" + query.annotate);
    //TODO : utility.trace('recieved an insert request from - ' + query.url);
    var urlparser = url.parse(query.url, true);
    //TODO : utility.trace(urlparser.protocol);
    var urlparser = url.parse(query.url, true);
    var twitter = query.twitter;

    var urlhash = getUrlHash(query.url);
    if (query.url.indexOf(serverName) < 0) {
        //TODO : utility.trace('insert request will be processed');
        if (urlparser.protocol == 'https:') {
            downloadHTTPS(query.url, function (rawData, ispdf) {
                DoArticleInsertion(req, res, query, rawData, urlhash, query.annotate, ispdf, query.title, twitter, '', '');
            });
            //https if loop ends , there is an issue with using the function callback that needs to be seen but as of now duplicacy here
        } else {
            downloadHTTP(query.url, function (rawData, ispdf, videourl, videotype) {
                if (ispdf) {
                    setTimeout(function () {
                        rawData = GetPdfText(rawData);
                        DoArticleInsertion(req, res, query, rawData, urlhash, query.annotate, ispdf, query.title, twitter, videourl, videotype);
                    }, 15000);
                    res.end();
                }
                else {
                    DoArticleInsertion(req, res, query, rawData, urlhash, query.annotate, ispdf, query.title, twitter, videourl, videotype);
                    res.end();
                }
            });
        }
    } else {
        res.end();
    }
});

app.use('/view', function (req, res, params) {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var text = query.txt;
    if (text != undefined && text != null) {
        text = text.toLowerCase();
    }
    //TODO : utility.trace('text value is ' + text);
    dataSink.findAll(function (error, skillArtifactCollection) {
        res.write(JSON.stringify(skillArtifactCollection));
        res.end();
    }, text);
});

app.use('/userview', function (req, res, params) {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var text = query.txt;
    if (text != undefined && text != null) {
        text = text.toLowerCase();
    }
    //TODO : utility.trace('text value is ' + text);
    dataSink.findAllArticlesPerUser(req, function (error, skillArtifactCollection) {
        res.write(JSON.stringify(skillArtifactCollection));
        res.end();
    }, text);
});

app.use('/showComments', function (req, res, params) {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var id = query.id;
    //TODO : utility.trace('show comments is being called ' + query);
    if (id != undefined && id != null) {
        //TODO : utility.trace('id value is ' + id);
        dataSink.findAllComments(function (error, commentCollection) {
            //TODO : utility.trace(commentCollection);
            res.write(JSON.stringify(commentCollection));
            res.end();
        }, id);
    }
});

app.use('/showProfiles', function (req, res, params) {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var id = query.id;

    if (id != undefined && id != null) {
        //TODO : utility.trace('id value is ' + id);
        dataSink.findAllProfiles(function (error, profileCollection) {
            //TODO : utility.trace(profileCollection);
            res.write(profileCollection[0].profile.first_name);
            res.end();
        }, 0, id);
    }
});

app.use('/addComment', function (req, res, params) {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var id = query.id;
    if (id != undefined && id != null) {
        //TODO : utility.trace('comment added for record with id ' + id);
    }

    var comment = query.comment;
    if (comment != undefined && comment != null) {
        //TODO : utility.trace(comment);
    }

    if (comment != undefined && comment.length > 0 && id != undefined && id.length > 0) {
        dataSink.saveComment({
            articleId: id,
            comment: comment
        }, function (error, docs) {
            if (error == null) {
                //TODO : utility.trace('comment has been added');
                res.end();
            }
        });
    }
});

app.use("/login", function (req, res, params) {
    //TODO : utility.trace('the blank request is being submitted');
    if (req.isAuthenticated()) {
        res.writeHead(200, { 'Set-Cookie': 'loggedId=' + req.session.loggedId });
        res.end(authenticatedContent.replace("#USER#", JSON.stringify(req.getAuthDetails().user)));
    }
    else {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        //TODO : utility.trace("this is the trace");
        res.end(unAuthenticatedContent.replace("#PAGE#", req.originalUrl));
    }
});


app.use("/getclusters", function (req, res, params) {
    //TODO : utility.trace("get the clusters");
    dataSink.findAll(function (error, skillArtifactCollection) {
        var computer = new WordBagComputer(skillArtifactCollection, utility);
        computer.CreatePoints(function (error, points) {
            kMeansCluster.ComputeClusters(10, points, function (points) {
                //TODO : utility.trace("____________________________________ASSOCIATED POINTS IN THE CLUSTER_____________________________________");

                for (var point in points) {
                    //TODO : utility.trace(points[point].articleUrl);
                    //TODO : utility.trace(points[point].association);
                    //TODO  : utility.trace(points[point].image);
                }
                //TODO : utility.trace("____________________________________ASSOCIATED POINTS IN THE CLUSTER END_____________________________________");
                res.write(JSON.stringify(points));
                res.end();
            });
        });
    }, null);
});

//TODO : utility.trace('about to start the server');
app.listen(3000);
