﻿// Import
var level = process.argv.indexOf('-d') === -1 ? 6 : 7;
var logger = new (require('caterpillar').Logger)({ level: level });
var filter = new (require('caterpillar-filter').Filter)();
var human = new (require('caterpillar-human').Human)();

// Pipe logger output to filter, then filter output to stdout
logger.pipe(filter).pipe(human).pipe(process.stdout);

var connect = require('connect');
var auth = require('../lib/index')
var url = require('url')
var fs = require('fs');
var datag = require('../routes/datagatherer.js').DataGatherer;
var utilities = require('../routes/utilities.js').Utility;
var https = require('https');
var http = require('http');
var crypto = require('crypto');
var md5 = crypto.createHash('md5');
var articlescore = require('../routes/articlescoring.js').ArticleScorer;
var utility = new utilities(logger);
var dataSink = new datag('localhost', 27017, function (obj) {
});

var scorer = new articlescore(utility, dataSink);

setInterval(function () {
    scorer.getModifiedArticles(function (error, collection) {
        if (error == null) {
            if (collection == null || collection.length == 0) {
            }
            else {
                scorer.evaluateScores(collection, function (error, collection) {
                    if (error != null) {
                    }
                });
            }
        }
        else {
        }
    });
}, 5000);

