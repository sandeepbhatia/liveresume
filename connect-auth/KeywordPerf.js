﻿// Import
var level = process.argv.indexOf('-d') === -1 ? 10 : 10;
var logger = new (require('caterpillar').Logger)({ level: level });
var filter = new (require('caterpillar-filter').Filter)();
var human = new (require('caterpillar-human').Human)();

// Pipe logger output to filter, then filter output to stdout
logger.pipe(filter).pipe(human).pipe(process.stdout);

var datag = require('../routes/datagatherer.js').DataGatherer;
var utilities = require('../routes/utilities.js').Utility;
var inference = require('../routes/keywordinference.js').KeywordInference;
var POSTagger = require('../routes/postagger.js').POSTagger;
var Lexer = require('../routes/lexer.js').Lexer;
var microtime = require('microtime')
var utility = new utilities(logger);
var keywordInference = new inference(utility);

var lexer = new Lexer(function () {
    //TODO : utility.trace('lexer created');
});
var index = 0;
var posTagger = new POSTagger();
var startsec;
var endsec;

function done(values) {
    endsec = microtime.now();
    console.log((endsec - startsec) / 1000);
    console.log(JSON.stringify(values));
}

function start() {
    var rawData = "Keyword extraction is an important technique for document retrieval, Web page re-trieval, document clustering, summarization, text mining, and so on. By extracting appropriate keywords, we can easily choose which document to read to learn therelationship among documents. A popular algorithm for indexing is the tdf mea-sure, which extracts keywords that appear frequently in a document, but that don't appear frequently in the remainder of the corpus. The term keyword extraction is used in the context of text mining, for example 15. A comparable research topic is called automatic term recognition in the context of computational linguistics and automatic indexing or automatic keyword extraction in information retrieval research.";

    startsec = microtime.now();

    keywordInference.setPerf(true);
    for(var i = 0; i < 100; )
    {
        keywordInference.scrubHtml(rawData, lexer, posTagger, false, function (error, sentences) {
            if (error == null) {
                //TODO : utility.trace('the analyze text will be now called');
                keywordInference.analyzeText(sentences, function (error, chiSquare) {
                    index++;
                    if (index == 100) {
                        done(chiSquare);
                    }
                });
            };
        });

        i = i + 1;
    }
}

start();
