﻿var Twit = require('twit')
// Import
var level = process.argv.indexOf('-d') === -1 ? 6 : 7;
var logger = new (require('caterpillar').Logger)({ level: level });
var filter = new (require('caterpillar-filter').Filter)();
var human = new (require('caterpillar-human').Human)();

// Pipe logger output to filter, then filter output to stdout
logger.pipe(filter).pipe(human).pipe(process.stdout);


var connect = require('connect');
var auth = require('../lib/index')
var url = require('url')
var fs = require('fs');
var datag = require('../routes/datagatherer.js').DataGatherer;
var utilities = require('../routes/utilities.js').Utility;
var inference = require('../routes/keywordinference.js').KeywordInference;
var POSTagger = require('../routes/postagger.js').POSTagger;
var KMeansCluster = require('../routes/kmeansclustering.js').KMeansCluster;
var WordBagComputer = require('../routes/computewordbags.js').WordBagComputer;
var Point = require('../routes/wordbagpoint.js').Point;
var Lexer = require('../routes/lexer.js').Lexer;
var https = require('https');
var http = require('http');
var posTagger = new POSTagger();

var crypto = require('crypto');
var md5 = crypto.createHash('md5');

var utility = new utilities(logger);
var keywordInference = new inference(utility);
var dataSink = new datag('localhost', 27017, function (obj) {
    //TODO : utility.trace('Mongo DB connection created');
});
var OAuth = require('oauth').OAuth;
var lexer = new Lexer(function () {
    //TODO : utility.trace('lexer created');
});

var notProcessed = false;
var twitterApp = new Twit({
    consumer_key: 'qY0Dmwushe9dUjy5BUPQ'
  , consumer_secret: 'lzBrFaKoyaccTv0c4za7tlgZ0UylAJ7m3sJ6dlY4wU'
  , access_token: '859217844-RhSBZHwEMaT8ywW4QlQKf0bVe5L9tfek2FzicJ5s'
  , access_token_secret: 'uHxz0mBFmR0eHn9jXS2RsSrd2EOA45R0npkheMdrOI'
});


var titles = [];

function isValidWord(textWord, tagTerm) {
    if (tagTerm == 'NNP') {
        if (textWord.trim().length > 2 && textWord.indexOf('-') < 0 && textWord.indexOf('][') < 0) {
            return true;
        }
    }


    return false;
};

function addNouns(sentence) {
    var words = lexer.lex(sentence);
    var taggedWords = posTagger.tag(words);

    if (taggedWords.length > 0) {
        for (var tagIndex = 0; tagIndex < taggedWords.length; tagIndex++) {
            var currentWord = taggedWords[tagIndex];
            var currentTag = currentWord[1];
            var currentTextWord = currentWord[0];
            if (isValidWord(currentTextWord, currentTag)) {
                titles.push(currentTextWord);
            }
        }
    }

}

function detectUrls(str) {
    var urls = [];
    //URL PATTERN (not case-sensitive)
    var p = /^(http(s)?|ftp):\/\/([a-z0-9_-]+\.)+([a-z]{2,}){1}(:|\/(.*))?$/i;
    var w = str.split(" "); //SPLITS STRING WITH SPACES TO ISOLATE URLS
    //SEARCHES THRU ENTIRE STRING TO FIND TEXTS MATCHING URL PATTERN
    for (i = 0; i < w.length; i++) {
        if (w[i].match(p)) //IF TEXT MATCHES PATTERN...
        {
            urls.push(w[i]);
        }
    }

    if (urls.length > 0) {
        logger.log(urls);
        AddArticlesData(urls);
    }
}

var downloadHTTP = function (file_url, callback) {

    var options = {
        host: 'localhost',
        port: 3000,
        path: '/insert?url=' + file_url + '&twitter=1',
        method: 'GET'
    };

    var htmlData = '';

    http.get(options, function (res) {
        res.on('data', function (data) {
            htmlData += data;
        }).on('end', function () {
                callback(htmlData, false);
        });
    });
};

var serverUrl = "http://localhost:3000/insert?url=";
function AddArticlesData(urls) {
    var url = '';
    for (var index = 0; index < urls.length; index++) {
        url = urls[index];

        if (url != null && url != undefined) {
            url = url;
            logger.log(url);
            downloadHTTP(url, function (data) {
                logger.log(data);
            });
        }
    }
}

setTimeout(function () {
    dataSink.findAll(function (error, skillArtifactCollection) {
        for (var index in skillArtifactCollection) {
            if (skillArtifactCollection[index].title != null && skillArtifactCollection[index].title != undefined) {
                addNouns(skillArtifactCollection[index].title);
            }
        }


        logger.log(titles);

        var stream = twitterApp.stream('statuses/filter', { track: titles })

        stream.on('tweet', function (tweet) {
            detectUrls(tweet.text);
            //logger.log(tweet.text)
        });
    });
}, 5000);
