/*
The intent of this snippet is to call the liveresume service to record the marked text and URLs.
*/
if (document != undefined) {
    //var serverUrl = "http://54.243.176.188:3000/insert?url=";
    var text = '';
    var serverUrl = "http://localhost:3000/insert?url=";
    var selection = document.getSelection();
    var ajax = null;
    var parentElement = null;
    var oldHtml = '';

    if (selection != null && selection != undefined) {
        text = selection;
    }

    var focusNode = selection.focusNode;
    if (focusNode != null && focusNode != undefined) {
        parentElement = focusNode.parentElement;
        oldHtml = parentElement.innerHTML;
        parentElement.style.backgroundColor = '#CCFF66';
    }

    var maxSize = 0;
    var maxSrc = '';
    $("img").each(function (index) {
        var width = $(this).width();
        var height = $(this).height();
        var src = $(this).attr('src');
        if (width == undefined || width == null) {
            width = 0;
        }
        if (height == undefined || height == null) {
            height = 0;
        }

        if ((width * height) > maxSize) {
            maxSize = width * height;
            maxSrc = src;
        }
    });

    ajax = new XMLHttpRequest();

    if (ajax != null && ajax != undefined) {
        ajax.open("GET", serverUrl + document.URL + "&text=" + text + "&imgsrc=" + maxSrc);
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("url=" + document.URL + "&text=" + text + "&imgsrc=" + maxSrc);
    }
    else {
        alert('we have not been able to connect to the server, contact support');
    }
}
else {
    alert('Please reinstall we are not able to detect valid browser objects to work with, contact support');
}
