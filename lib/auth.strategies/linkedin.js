/*!
 * Copyright(c) 2010 Stephen Belanger <admin@stephenbelanger.com>
 * MIT Licensed
 */
var OAuth = require("oauth").OAuth,
	url = require("url"),
	connect = require("connect"),
	http = require('http');

Linkedin = module.exports = function (options, server) {
    options = options || {}

    var dataSink = options.datasink;
    var utilities = options.utilities;
	var that = {};
	var my = {};

	// Construct the internal OAuth client
	my._oAuth = new OAuth(
		"https://api.linkedin.com/uas/oauth/requestToken?scope=r_fullprofile%20r_emailaddress"
		, "https://api.linkedin.com/uas/oauth/accessToken"
		, options.consumerKey
		, options.consumerSecret
		, "1.0"
		, options.callback || null
		, "HMAC-SHA1"
	);

	// Give the strategy a name
	that.name = options.name || "linkedin";
	
	// Build the authentication routes required 
	that.setupRoutes= function( app ) {
		app.use('/auth/linkedin_callback', function(req, res) {
			req.authenticate([that.name], function(error, authenticated) {
				res.writeHead(303, {
					'Location': req.session.linkedin_redirect_url
				});
				res.end('');
			});
		});
	}

	// Declare the method that actually does the authentication
	that.authenticate = function(request, response, callback) {
		//todo: if multiple connect middlewares were doing this, it would be more efficient to do it in the stack??
		var parsedUrl = url.parse(request.originalUrl, true);

		//todo: makw the call timeout ....
		var self = this;
		if (parsedUrl.query && parsedUrl.query.oauth_token && parsedUrl.query.oauth_verifier && request.session.auth["linkedin_oauth_token_secret"]) {
      self.trace( 'Phase 2/2 : Requesting an OAuth access token.' );
			my._oAuth.getOAuthAccessToken(parsedUrl.query.oauth_token, request.session.auth["linkedin_oauth_token_secret"], parsedUrl.query.oauth_verifier, function(error, token, secret, params) {
					if (error) {
						callback(null);
					} else {
						request.session.auth["linkedin_oauth_token_secret"] = secret;
						request.session.auth["linkedin_oauth_token"] = token;

                        var linkedinProfileURL = "https://api.linkedin.com/v1/people/~:";
                        var fieldSelectors = "(id,picture-url,site-standard-profile-request,first-name,last-name,headline,location:(name),industry,specialties,positions,honors,interests,publications,patents,languages,skills,certifications,educations,courses,three-current-positions,three-past-positions,following,date-of-birth,summary)";
						var linkedInFullUrl = linkedinProfileURL + fieldSelectors + "?format=json";
						// Get user profile data.
						my._oAuth.getProtectedResource(linkedInFullUrl, 'get', token, secret, function (error, data, response) {
							if (error) {
								self.fail(callback);
							} else {
								var result = JSON.parse(data);
								var user = {
									first_name: result.firstName
									, last_name: result.lastName
									, url: result.siteStandardProfileRequest.url
									, headline: result.headline
                                    , summary: result.summary
                                    , id: result.id
								};
                                var savedUser = {};
                                savedUser.type = 0;
                                savedUser.id = user.id;
                                savedUser.profile = user;
                                utilities.setSession(user.id, request);
                                dataSink.saveProfile(savedUser,function (error, profile) {
                                    if (error == null) {
                                        //TODO : console.log(JSON.stringify(profile));
                                        //TODO : console.log("___________________________________________________________________________________________");
                                    }
                                });
								self.executionResult.user = user; 
								self.success(user, callback);
                                //TODO : console.log(data);
							}
						})
					}
			});
		} else {
      self.trace( 'Phase 1/2 - Requesting an OAuth Request Token' )
			my._oAuth.getOAuthRequestToken(function(error, token, secret, auth_url, params) {
				if (error) {
					callback(null); // Ignore the error upstream, treat as validation failure.
				} else {
					request.session['linkedin_redirect_url'] = request.originalUrl;
					request.session.auth["linkedin_oauth_token_secret"] = secret;
					request.session.auth["linkedin_oauth_token"] = token;
					self.redirect(response, "https://api.linkedin.com/uas/oauth/authorize?oauth_token=" + token, callback);
				}
			});
		}
	}	
	return that;
};

