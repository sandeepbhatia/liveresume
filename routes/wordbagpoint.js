﻿function Point(data) {
    this.data = data;
    this.clusterId = -1;
    this.association = -1;
    this.clusterDistance = -1;
    this.articleUrl = '';
    this.image = null;
}

exports.Point = Point;