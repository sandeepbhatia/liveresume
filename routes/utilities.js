CodeMarker = function () {
    this.nextId = 0;
    this.elements = {};
}

CodeMarker.prototype.IsEmpty = function () {
    if (this.elements.length == 0) {
        return true;
    }

    return false;
}

CodeMarker.prototype.Push = function (marker) {
    var time = new Date();
    var id = this.nextId;

    this.elements[this.nextId] = { identifier: marker, start: time };
    if (this.nextId == Number.MAX_VALUE) {
        this.nextId = 0;
    }
    else {
        this.nextId++;
    }

    return id;
}

CodeMarker.prototype.Pop = function (marker, id) {
    var end = new Date();
    var start = this.elements[id];

    if (start != undefined && start != null) {
        return end - start['start'];
    }

    return null;
}

//the constructor for data gatherer this accepts the host, port and the callback on connection
Utility = function (logger, perf) {
    if (this.that != undefined && this.that != null) {
        return this.that;
    }
    else {
        this.enabled = false;
        this.that = this;
        this.codeMarker = new CodeMarker();
        this.logger = logger;
    }
}

Utility.prototype.push = function (marker) {
    var id = this.codeMarker.Push(marker);
    return id;
}

Utility.prototype.pop = function (marker, id) {
    var difference = this.codeMarker.Pop(marker, id);
    if (difference > 1000) {
        this.logger.log('warn',  marker + ':' + difference);
    }
    else {
        this.logger.log('info',  marker + ':' + difference);
    }
    return difference;
}

Utility.prototype.isTracingEnabled = function () {
    return this.enabled;
}

Utility.prototype.getUser = function (req) {
    var cookies = {};
    req.headers.cookie && req.headers.cookie.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        cookies[parts[0].trim()] = (parts[1] || '').trim();
    });

    if (cookies['loggedId'] == undefined || cookies['loggedId'].length == 0) {
        return null;
    }

    return cookies['loggedId'];
}

Utility.prototype.enableTracing = function () {
    global.traceObject = this;
    this.enabled = true;
};

Utility.prototype.setSession = function (id, request) {
    var session = request.session;
    session.loggedId = id;
}

Utility.prototype.trace = function () { };
Utility.prototype.traceCollection = function () { };
exports.Utility = Utility;
