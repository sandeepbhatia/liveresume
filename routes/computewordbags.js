﻿var natural = require('natural');
var WNdb = require('WNdb');
var Point = require('./wordbagpoint.js').Point;
var utilities = require('./utilities.js').Utility;

var utility = new utilities();

function WordBagComputer(ArticleCollection, utilityObj) {
    this.articleCollection = ArticleCollection;
    utility = utilityObj;
}


WordBagComputer.prototype.CreatePoints = function (callback) {
    var points = [];
    if (this.articleCollection == null || this.articleCollection.length == 0) {
        callback(null, []);
    }

    var wordnet = new natural.WordNet();
    var countZeroWordArticles = 0;
    for (var index in this.articleCollection) {
        var currentArticle = this.articleCollection[index];
        var words = currentArticle.chi;
        var data = [];
        var currentProcessedWord = 0;
        if (words.length == 0) {
            countZeroWordArticles = countZeroWordArticles + 1;
        }

        for (var i = 0; i < words.length; i++) {
            if (data.indexOf(words[i]) == -1) {
                data.push(words[i]);
                wordnet.lookup(words[i], function (results) {
                    results.forEach(function (result) {
                        for (var eachsynonym in result) {
                            data.push(eachsynonym);
                        }
                    });
                });
            }

            if (i == words.length - 1) {
                data.sort();
                newPoint = new Point(data);
                newPoint.articleUrl = currentArticle.url;
                newPoint.image = currentArticle.img;
                points.push(newPoint);
                if (points.length == (this.articleCollection.length - countZeroWordArticles)) {
                    callback(null, points);
                }
            }
        }
     
    }
}

exports.WordBagComputer = WordBagComputer;