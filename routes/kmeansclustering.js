﻿var Point = require('./wordbagpoint.js').Point;
var utility;

/*****************************************************************************************************************************************
Clustering constructor
*****************************************************************************************************************************************/
function KMeansCluster(utilityObj) {
    utility = utilityObj;
}

/*****************************************************************************************************************************************
The main method for computing clusters. this takes the k parameter that is no of clusters, points the data array that has the data to 
cluster, callback the function that is called after the clustering is done
*****************************************************************************************************************************************/
KMeansCluster.prototype.ComputeClusters = function (k, points, callback) {
    var clusterAssociationBuckets;
    var noOfPoints = points.length;
    var clusterMeanIndex = [];
    var clusterCenters = [];
    var moved = true;
    var maxallowed = 100;
    var iteration = 0;

    if (points.length == undefined || points.length == 0) {
        return;
    }

    for (var index = 0; index < k; index++) {
        clusterMeanIndex.push(Math.floor(Math.random() * noOfPoints));
        clusterCenters.push(points[clusterMeanIndex[index]]);
    }

    while (moved && iteration < maxallowed) {
        clusterAssociationBuckets = this.ComputeClusterAssociations(points, clusterCenters);

        var sumData = [];
        var averageData = [];
        var newClusterCenters = [];

        var sumdata = {};

        for (index = 0; index < clusterAssociationBuckets.length; index++) {

            if (clusterAssociationBuckets[index].length > 0) {
                sumdata = clusterAssociationBuckets[index][0].data;
                sumdata = clusterAssociationBuckets[index][0].data;
                for (var pointIndex = 1; pointIndex < clusterAssociationBuckets[index].length; pointIndex++) {
                    sumdata = this.Aggregate(sumdata, clusterAssociationBuckets[index][pointIndex].data);
                }

                averageData = this.Average(sumdata, clusterAssociationBuckets[index].length);

                newClusterCenters[index] = averageData;
            }
            else {
                newClusterCenters[index] = new Point([]);
            }
        }

        moved = this.CheckMoved(newClusterCenters, clusterCenters);
        iteration++;
        clusterCenters = newClusterCenters;
    }

    callback(points);

}

/*****************************************************************************************************************************************
Method checks the cluster centers have moved or not, if not we exit with the cluster that is present
*****************************************************************************************************************************************/
KMeansCluster.prototype.CheckMoved = function (newClusterCenters, clusterCenters) {
    for (var clusterIndex = 0; clusterIndex < clusterCenters.length; clusterIndex++) {
        if (this.ComputeDistance(clusterCenters[clusterIndex], newClusterCenters[clusterIndex]) > 0) {
            return true;
        }
    }

    return false;
}

/*****************************************************************************************************************************************
Aggregate the points, this is just calculating superset of the data
*****************************************************************************************************************************************/
KMeansCluster.prototype.Aggregate = function (sumData, data) {
    //check if this logic is correct
    if (sumData == undefined || sumData == null) {
        sumData = [];
    }

    if (data == undefined || data == null) {
        return sumData;
    }
    sumData.concat(data);
    return sumData;

}

/*****************************************************************************************************************************************
For the purpose of the articles this would be same as aggregate                                                                                                    
*****************************************************************************************************************************************/
KMeansCluster.prototype.Average = function (sumData, N) {
    //check if this logic is correct
    if (sumData == undefined || sumData == null) {
        sumData = [];
    }
    var average = sumData;
    return new Point(average);
}

/*****************************************************************************************************************************************
//This can be optimized to scan the first and second only once. 
what it does is calculate the different words in two entities and the count is the calc of the distance
*****************************************************************************************************************************************/
KMeansCluster.prototype.ComputeDistance = function (first, second) {

    var wordmap = {};

    var index = 0;

    var data = first.data;
    while (index < data.length) {
        wordmap[data[index]] = 1;
        index++;
    }
    data = second.data;
    index = 0;
    while (index < data.length) {
        if (wordmap[data[index]] == undefined || wordmap[data[index]] == null) {
            wordmap[data[index]] = 2;
        }
        else {
            if (wordmap[data[index]] == 1) {
                wordmap[data[index]] = 3;
            }
        }

        index++;
    }
    var distance = 0;
    for (wordmapelement in wordmap) {
        if (wordmap[wordmapelement] == 1 || wordmap[wordmapelement] == 2) {
            distance++;
        }
    }

    return distance;
}

/*****************************************************************************************************************************************
The method finds the associations between the points and their related cluster centers
*****************************************************************************************************************************************/
KMeansCluster.prototype.ComputeClusterAssociations = function (points, clusterCenters) {
    clusterAssociationBuckets = [];

    for (var index = 0; index < clusterCenters.length; index++) {
        //TODO : utility.trace('cluster at : ' + index + ':' + JSON.stringify(clusterCenters[index]));
        clusterAssociationBuckets[index] = [];
    }

    for (var pointIndex = 0; pointIndex < points.length; pointIndex++) {

        candidateClusterIndex = 0;
        candidateClusterDistance = this.ComputeDistance(clusterCenters[0], points[pointIndex]);

        for (var index = 1; index < clusterCenters.length; index++) {

            //for each and every point we will compute best cluster its associated with 
            var currentClusterMeanPoint = clusterCenters[index];

            var currentClusterDistance = this.ComputeDistance(clusterCenters[index], points[pointIndex]);

            if (currentClusterDistance < candidateClusterDistance) {
                candidateClusterDistance = currentClusterDistance;
                candidateClusterIndex = index;
            }
        }

        points[pointIndex].clusterDistance = candidateClusterDistance;
        points[pointIndex].association = candidateClusterIndex;
        clusterAssociationBuckets[candidateClusterIndex].push(points[pointIndex]);
    }

    return clusterAssociationBuckets;
}


//this is not a blank class methods are added at locations where they are appropriate
exports.KMeansCluster = KMeansCluster;