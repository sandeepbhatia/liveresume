﻿ArticleScorer = function (utility, datasink) {
    this.utility = utility;
    this.dataSink = datasink;
}

ArticleScorer.prototype.getModifiedArticles = function (callback) {
    this.dataSink.findModifiedArticles(function (error, collection) {
        callback(null, collection);
    });
}

ArticleScorer.prototype.GetWeights = function () {
    var weights = {};
    weights.BaseScore = 1;
    weights.NoOfUsersForArticle = 1;
    weights.NoOfLikesForArticle = 10;
    weights.CommentsForArticle = 50;
    return weights;
}

ArticleScorer.prototype.GetFactors = function () {
    var factors = {};
    factors.BaseScore = function () { return 10; }
    factors.NoOfUsersForArticle = function (x) { if (x == 0) x = 1; return Math.log(x); }
    factors.NoOfLikesForArticle = function (x) { if (x == 0) x = 1; return Math.log(x); }
    factors.CommentsForArticle = function (x) { if (x == 0) x = 1;  return Math.log(x); }
    return factors;
}

ArticleScorer.prototype.evaluateScores = function (collection, callback) {
    var id = -1;
    try {
        id = this.utility.push('evaluate scores');
        var hash = null;
        var weights = this.GetWeights();
        var factors = this.GetFactors();
        var scoringSum = weights.BaseScore * factors.BaseScore();
        var self = this;

        for (var index in collection) {
            hash = collection[index].hash;
            self.dataSink.getTotalUsersForArticle(hash, function (count) {
                scoringSum += weights.NoOfUsersForArticle * factors.NoOfUsersForArticle(count);
                self.dataSink.getTotalLikesForArticle(hash, function (count) {
                    scoringSum += weights.NoOfLikesForArticle * factors.NoOfLikesForArticle(count);
                    self.dataSink.getTotalCommentsForArticle(hash, function (count) {
                        scoringSum += weights.CommentsForArticle * factors.CommentsForArticle(count);
                        self.dataSink.addArticleScore(hash, scoringSum, function (artifact) {
                            if (scoringSum == null) {
                                self.dataSink.queueArticleForScoring(hash);
                            }
                        });
                    });
                });
            });
        }
    }
    finally {
        this.utility.pop('evaluate scores', id);
    }
}


exports.ArticleScorer = ArticleScorer;
