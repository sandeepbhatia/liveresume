var Database = require('mongodb').Db;
var Connection = require('mongodb').Connection;
var Server = require('mongodb').Server;
var BSON = require('mongodb').BSON;
var ObjectID = require('mongodb').ObjectID;
var skillCollection = 'SkillArtifactsCollection';
var commentCollection = 'CommentCollection';
var profileCollection = 'ProfileCollection';
var articleUniqueCollection = 'ArticleCollection';
var userArticleCollection = 'UserArticleCollection';
var mongodb = require('mongodb');

//the annotation tagging
var inference = require('./keywordinference.js').KeywordInference;
var POSTagger = require('./postagger.js').POSTagger;
var Lexer = require('./lexer.js').Lexer;
var keywordInference = new inference();
var lexer = new Lexer(function () {
});
var posTagger = new POSTagger();


//the constructor for data gatherer this accepts the host, port and the callback on connection
DataGatherer = function (host, port, callback) {
    this.db = new Database('liveresume', new Server(host, port, { auto_reconnect: true , j: true, w: 1}, {}));
    this.db.open(function (error, client) {
        if (error == null) {
            this.clientConnection = client;
        }
    });
    callback(this.db);
}

//gets the skill artifacts collection that is collection populated when the browser annotates content
DataGatherer.prototype.GetArtifactsCollection = function (callback, text, isarray) {
    this.db.collection(skillCollection, function (error, skillCollection) {
        if (error) {
            callback(error);
        }
        else {
            if (text == undefined || text == null || (!isarray && (text.trim().length == 0))) {
                skillCollection.find().toArray(function (error, results) {
                    if (error) {
                        callback(error);
                    }
                    else {
                        callback(null, results);
                    }
                });
            }
            else {
                if (isarray != undefined && isarray != null) {
                    skillCollection.find({ 'chi': { $in: text } }, { limit: 10 }).toArray(function (err, docs) {
                        callback(null, docs);
                    });
                }
                else {
                    skillCollection.find({ 'chi': text }, { limit: 10 }).toArray(function (err, docs) {
                        callback(null, docs);
                    });
                }
            }
        }
    });
};

DataGatherer.prototype.getCommentsCollection = function (callback, id) {
    this.db.collection(commentCollection, function (error, commentCollection) {
        if (error) {
            callback(error);
        }
        else {
            commentCollection.find({ 'articleId': id }, { limit: 100 }).toArray(function (err, docs) {
                callback(null, docs);
            });
        }
    });
};


//finds all that is stored in the skill artifacts collection. Method is not to be used in production 
//directly, this is here for initial boot strapping only
DataGatherer.prototype.findAll = function (callback, text, isarray) {
    this.GetArtifactsCollection(function (error, collection) {
        if (error) {
            callback(error);
        }
        else {
            callback(null, collection);
        }
    }, text, isarray);
};

DataGatherer.prototype.findArticleByHash = function (hash, callback) {
    this.db.collection(skillCollection, function (error, skillCollection) {
        skillCollection.find({ 'hash': hash }, { limit: 1 }).toArray(function (err, docs) {
            if (docs.length > 0) {
                callback(null, docs[0]);
            }
            else {
                callback(null, null);
            }
        });
    });
}

DataGatherer.prototype.findModifiedArticles = function (callback) {
    this.db.collection(skillCollection, function (error, skillCollection) {
        skillCollection.find({ 'modified': 1 }).toArray(function (err, docs) {
            if (docs.length > 0) {
                callback(null, docs);
            }
            else {
                callback(null, null);
            }
        });
    });
}

//finds all that is stored in the skill artifacts collection. Method is not to be used in production 
//directly, this is here for initial boot strapping only
DataGatherer.prototype.findAllComments = function (callback, id) {
    this.getCommentsCollection(function (error, collection) {
        if (error) {
            callback(error);
        }
        else {
            callback(null, collection);
        }
    }, id);
};


DataGatherer.prototype.SaveUserAssociation = function (req, skillArtifact, callback) {
    var that = this;
    var hash = skillArtifact.hash;
    var userId = req.session.loggedId;
    var annotationText = skillArtifact.annotate;
    this.db.collection(userArticleCollection, function (error, userArticleCollection) {
        if (error) {
            callback(error);
        }
        else {
            userArticleCollection.remove({ UserId: userId, ArticleHash: hash }, function (error) {
                //do we maintain only last annotation or all annotations change acc
                userArticleCollection.insert({ UserId: userId, ArticleHash: hash, annotation: annotationText }, function (error) {
                    if (error == null) {
                        that.db.collection(skillCollection, function (error, skillCollection) {
                            if (error == null) {
                                //TODO : console.log('the find and modify is being called');
                                skillCollection.findAndModify({ hash: hash },
                                    [],
                                 { $set: { modified: 1 } },
                                 function (error) {
                                     if (error == null) {
                                         //TODO : console.log('the skill artifact is about to make a call back');
                                         callback(skillArtifact);
                                     }
                                 });
                            }
                        });
                    }
                });
            });
        }
    });
}

//The method is responsible for saving the current Skill Artifact, The structure in the
//collection is as follows {_id, data, title, summary, url, personId, skillId, created_at}
DataGatherer.prototype.SaveSkillArtifact = function (req, SkillArtifact, callback) {
    var that = this;
    that.db.collection(skillCollection, function (error, skillCollection) {
        skillCollection.insert(SkillArtifact, function (error) {
            if (error == null) {
                callback(null, SkillArtifact);
            }
        });
    });
}

//The method is responsible for saving the current Skill Artifact, The structure in the
//collection is as follows {_id, data, title, summary, url, personId, skillId, created_at}
DataGatherer.prototype.saveComment = function (comment, callback) {
    var self = this;
    this.db.collection(commentCollection, function (error, commentCollection) {
        if (error) {
            callback(error);
        }
        else {
            commentCollection.insert(comment, function () {
                self.db.collection(skillCollection, function (error, skillCollection) {
                    if (error == null) {
                        //TODO : console.log('the find and modify is being called');
                        skillCollection.findAndModify({ hash: comment.articleId },
                            [],
                         { $set: { modified: 1 } },
                         function (error) {
                             if (error == null) {
                                 //TODO : console.log('comment was added so article marked modified again');
                             }
                         });
                    }
                });
                callback(null, comment);
            });
        }
    });
}


DataGatherer.prototype.queueArticleForScoring = function (hash) {
    this.db.collection(skillCollection, function (error, skillCollection) {
        if (error == null) {
            //TODO : console.log('the find and modify is being called');
            skillCollection.findAndModify({ hash: hash },
                [],
             { $set: { modified: 1 } },
             function (error) {
                 if (error == null) {
                     //TODO : console.log('article queued for rescoring');
                 }
             });
        }
    });
}

DataGatherer.prototype.saveProfile = function (profile, callback) {
    this.db.collection(profileCollection, function (error, profileCollection) {
        if (error) {
            callback(error);
        }
        else {
            profileCollection.insert(profile, function () {
                callback(null, profile);
            });
        }
    });
}

DataGatherer.prototype.getProfile = function (callback, type, id) {
    this.db.collection(profileCollection, function (error, profileCollection) {
        if (error) {
            callback(error);
        }
        else {
            profileCollection.find({ '$and': [{ 'id': id }, { 'type': type }] }).toArray(function (err, docs) {
                //TODO : console.log(docs);
                callback(null, docs);
            });
        }
    });
};

DataGatherer.prototype.findAllProfiles = function (callback, type, id) {
    this.getProfile(function (error, collection) {
        if (error) {
            callback(error);
        }
        else {
            callback(null, collection);
        }
    }, type, id);
};

DataGatherer.prototype.getTotalUsersForArticle = function (hash, callback) {
    this.db.collection(userArticleCollection, function (error, userArticleCollection) {
        userArticleCollection.find({ 'ArticleHash': hash }).toArray(function (err, docs) {
            if (docs != null || docs.length > 0) {
                callback(docs.length);
            }
            else {
                callback(1);
            }
        });
    });
}

DataGatherer.prototype.getTotalCommentsForArticle = function (hash, callback) {
    this.findAllComments(function (error, comments) {
        if (comments.length > 0) {
            callback(comments.length);
        }
        else {
            callback(1);
        }
    },
    hash);
}

DataGatherer.prototype.getTotalLikesForArticle = function (hash, callback) {
    //TODO : temporary likes are not stored in the db as of now 
    callback(1);
}

DataGatherer.prototype.addArticleScore = function (hash, score, callback) {
    this.db.collection(skillCollection, function (error, skillCollection) {
        if (error == null) {
            //TODO : console.log('the find and modify is being called');
            skillCollection.findAndModify({ hash: hash },
                [],
             { $set: { evaluatedScore: score, modified: 0 } },
             function (error) {
                 if (error == null) {
                     //TODO : console.log('the skill artifact is about to make a call back');
                     callback({ hash: hash, evaluatedScore: score });
                 }
             });
        }
        else {
            callback(null);
        }
    });
}

DataGatherer.prototype.findAllArticlesPerUser = function (req, callback, text) {
    var that = this;
    var userId = req.session.loggedId;
    var chivalue = null;
    var userannotation = null;

    if (userId == null || userId == undefined) {
        return [];
    }

    this.db.collection(userArticleCollection, function (error, userArticleCollection) {
        var articles = [];
        var articleObjects = {};
        var chhindex = 0;

        userArticleCollection.find({ UserId: userId }, { ArticleHash: 1, annotation: 1, _id: 0 }).toArray(function (error, articleHashes) {
            for (var index = 0; index < articleHashes.length; index++) {
                articles.push(articleHashes[index].ArticleHash);
                articleObjects[articleHashes[index].ArticleHash] = articleHashes[index];
            }
            that.db.collection(skillCollection, function (error, skillCollection) {
                if (error == null) {
                    skillCollection.find({ 'hash': { $in: articles } }, { limit: 100 }).toArray(function (err, docs) {
                        if (docs.length > 0) {
                            for (var index = 0; index < docs.length; index++) {
                                userannotation = articleObjects[docs[index].hash];
                                chivalue = docs[index].chi;
                                keywordInference.getAnnotationScore(userannotation, lexer, posTagger, chivalue, function (error, score) {
                                    docs[index].scoreArticle = score;
                                    if (index == docs.length - 1) {
                                        callback(null, docs);
                                    }
                                });
                            }
                        }
                    });
                }
            });

        });
    });
}

exports.DataGatherer = DataGatherer;

