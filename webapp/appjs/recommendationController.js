var sharedScope;

function onClick() {
    var txt = document.getElementById('url');
    sharedScope.updateResults(txt.value);
};


function recommendationController($scope, $rootScope, $http) {
    sharedScope = $scope;
    $http.get('/getrecos').success(function (data) {
        $scope.resumelets = data;
    });

    $scope.updateResults = function (txtValue) {
        if (txtValue != undefined && txtValue != null) {
            $http.get('/getrecos?txt=' + txtValue).success(function (data) {
                $scope.resumelets = data;
            });
        }
    }
}
