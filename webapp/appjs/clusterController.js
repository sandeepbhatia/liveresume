﻿var sharedScope;

function clusterController($scope, $rootScope, $http) {
    sharedScope = $scope;
    $http.get('/getclusters').success(function (data) {
        var clusters1, clusters2, clusters3, clusters4, clusters5, clusters6, clusters7, clusters8, clusters9, clusters10;
        clusters1 = [];
        clusters2 = [];
        clusters3 = [];
        clusters4 = [];
        clusters5 = [];
        clusters6 = [];
        clusters7 = [];
        clusters8 = [];
        clusters9 = [];
        clusters10 = [];

        for (var record in data) {
            
            var recrd = data[record];
            if (recrd.association == 0) {
                clusters1.push(recrd);
            }
            if (recrd.association == 1) {
                clusters2.push(recrd);
            }
            if (recrd.association == 2) {
                clusters3.push(recrd);
            }
            if (recrd.association == 3) {
                clusters4.push(recrd);
            }
            if (recrd.association == 4) {
                clusters5.push(recrd);
            }
            if (recrd.association == 5) {
                clusters6.push(recrd);
            }
            if (recrd.association == 6) {
                clusters7.push(recrd);
            }
            if (recrd.association == 7) {
                clusters8.push(recrd);
            }
            if (recrd.association == 8) {
                clusters9.push(recrd);
            }
            if (recrd.association == 9) {
                clusters10.push(recrd);
            }

            $scope.clusters1 = clusters1;
            $scope.clusters2 = clusters2;
            $scope.clusters3 = clusters3;
            $scope.clusters4 = clusters4;
            $scope.clusters5 = clusters5;
            $scope.clusters6 = clusters6;
            $scope.clusters7 = clusters7;
            $scope.clusters8 = clusters8;
            $scope.clusters9 = clusters9;
            $scope.clusters10 = clusters10;
        }
    });
}
