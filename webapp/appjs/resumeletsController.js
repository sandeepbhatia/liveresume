var sharedScope;

function addComment(url, id) {
    var textComment = document.getElementById(id);
    sharedScope.addComment(id, textComment.value);
}

function showComments(id) {
    var textComment = document.getElementById(id);
    sharedScope.showComments(id);
}

function searchValueChanged() {
    var txt = document.getElementById('search');
    sharedScope.updateResults(txt.value);
};

function resumeletsController($scope, $rootScope, $http) {
    sharedScope = $scope;
    $http.get('/view').success(function (data) {
        $scope.resumelets = data;
    });

    $scope.updateResults = function (txtValue) {
        if (txtValue == undefined || txtValue == null) {
            $http.get('/view').success(function (data) {
                $scope.resumelets = data;
            });
        }
        else {
            $http.get('/view?txt=' + txtValue).success(function (data) {
                $scope.resumelets = data;
            });
        }
    }

    $scope.addComment = function (id, comment) {
        if (comment != undefined && comment != null) {
            $http.get('/addComment?id=' + id + '&comment=' + comment).success(function (data) {
            });
        }
    }

    $scope.showComments = function (id) {
        if (id != undefined && id != null) {
            $http.get('/showComments?id=' + id).success(function (data) {
                $scope.comments = data;
            });
        }
    }
    /*$scope.resumelets = [
    {text:'learn angular', done:true},
    {text:'build an angular app', done:false}]; */

    $scope.addResumelet = function () {
        $scope.resumelets.push({ text: $scope.resumeletText, done: false });
        $scope.resumeletText = '';
    };

    $scope.remaining = function () {
        var count = 0;
        angular.forEach($scope.resumelets, function (resumelet) {
            count += resumelet.done ? 0 : 1;
        });
        return count;
    };

    $scope.archive = function () {
        var oldResumelets = $scope.resumelets;
        $scope.resumelets = [];
        angular.forEach(oldResumelets, function (resumelet) {
            if (!resumelet.done) $scope.resumelets.push(resumelet);
        });
    };
}
