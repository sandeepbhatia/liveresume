var natural = require('natural');

NaiveBayesClassifier = function (persistedClassifier) {
    if (persistedClassifier != undefined && persistedClassifier != null) {
        natural.BayesClassifier.load(persistedClassifier, null, function (err, classifier) {
            if (err == null || err == undefined) {
                this.classifier = classifier;
            }
        });
    } else {
        this.classifier = new natural.BayesClassifier();
    }
};

NaiveBayesClassifier.prototype.trainClassifier = function (content, classification, callback) {
    this.classifier.addDocument(content, classification);
    this.classifier.train();
    callback(null);
};

NaiveBayesClassifier.prototype.classifyContents = function (content, callback) {
    var classification = this.classifier.classify(content);
    callback(null, classification);
};

NaiveBayesClassifier.prototype.persistClassifier = function (callback) {
    this.classifier.save('naivebayesclassifier.json', function (err, classifier) {
        if (err != null) {
            //handle errors here
        }
    });
};

exports.NaiveBayesClassifier = NaiveBayesClassifier;
                                        
