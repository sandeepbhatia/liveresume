/*ii
   The intent of this snippet is to call the liveresume service to record the marked text and URLs.
 */

//var serverUrl = "http://54.243.176.188:3000/insert?url=";
var text = '';
var serverUrl = "http://localhost:3000/insert?url=";
var selection = document.getSelection();
var parentElement = null;
var oldHtml = '';
var maxSize = 0;
var maxSrc = '';
var ajax = null;
var divNode = null;
function onSubmitToAliveResume(document) {
    var annotate = document.getElementById('__aliveresumeannotate__');
    ajax = new XMLHttpRequest();
    ajax.withCredentials = true;
    if (ajax != null && ajax != undefined) {
        ajax.open("GET", serverUrl + document.URL + "&annotate=" + annotate.value  + "&title=" + document.title + "&imgsrc=" + maxSrc);
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("url=" + document.URL + "&annotate=" + annotate.value + "&title=" + document.title + "&imgsrc=" + maxSrc);
        divNode.style.visibility = "hidden";
    }
    else {
        alert('we have not been able to connect to the server, contact support');
    }
};


if (document != undefined) {

    if (selection != null && selection != undefined) {
        text = selection;
    }

    var images = document.getElementsByTagName('img');
    for (var i = 0, max = images.length; i < max; i++) {
        var width = images[i].width;
        var height = images[i].height;
        var src = images[i].src;
        if (width == undefined || width == null) {
            width = 0;
        }
        if (height == undefined || height == null) {
            height = 0;
        }

        if ((width * height) > maxSize) {
            maxSize = width * height;
            maxSrc = src;
        }
    }

    divNode = document.getElementById('__aliveprofile__');
    if (divNode == null || divNode == undefined) {
        divNode = document.createElement("div");
        divNode.setAttribute("id", "__aliveprofile__");
        divNode.style.position = "absolute";
        divNode.style.pixelTop = (screen.height / 2) - 250;
        divNode.style.pixelLeft = 0;
        divNode.style.pixelWidth = 500;
        divNode.style.pixelHeight = 300;
        divNode.style.backgroundColor = "Green";
        divNode.style.zindex = 100;
        var title = document.title;
        divNode.innerHTML = "<table border='1' cellpadding='10' style='width: 100%;height: 100%' ><tr><td height='10%' style='color:#FFFFFF'>" + title + "</td></tr><tr><td height='10%' style='color:#FFFFFF'>Add Some Annotation reflecting understanding of this article</td></tr><tr><td height='90%; align: center'><textarea style='width: 95%; align: center; height: 90%; font-family: Arial, Helvetica, sans-serif; font-size: large; font-weight: normal; font-style: normal; font-variant: normal; color: #008000;' name='__aliveresumeannotate__' id='__aliveresumeannotate__' rows='2'></textarea></td></tr><tr><td><input id='aliveresumesubmit' style='font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; color: #000000' type='button' onclick='onSubmitToAliveResume(document);' value='Send to ALiveResume' /></td></tr></table>";
        document.body.appendChild(divNode);
    }
    else {
        divNode.style.visibility = "visible";
    }

}
else {
    alert('Please reinstall we are not able to detect valid browser objects to work with, contact support');
}
