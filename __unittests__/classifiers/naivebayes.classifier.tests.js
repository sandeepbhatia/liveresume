var NaiveBayesClassifier = require('./../../classifiers/naivebayes.client.js').NaiveBayesClassifier;

var classifier = new NaiveBayesClassifier();

exports['trainclassifier'] = function (test) {
    var oncomplete = function () { };
    classifier.trainClassifier('dog bitch', 'spam', oncomplete);
    classifier.trainClassifier('swear', 'spam', oncomplete);
    classifier.trainClassifier('love sex and dhoka', 'spam', oncomplete);
    classifier.trainClassifier('zillionaire!!', 'spam', oncomplete);
    classifier.trainClassifier('free swear', 'spam', oncomplete);
    classifier.trainClassifier('kill bitch', 'spam', oncomplete);


    classifier.trainClassifier('c++ programming', 'nospam', oncomplete);
    classifier.trainClassifier('art form', 'nospam', oncomplete);
    classifier.trainClassifier('vlsi design', 'nospam', oncomplete);
    classifier.trainClassifier('love programming', 'nospam', oncomplete);
    classifier.trainClassifier('hate lazy coding', 'nospam', oncomplete);
    classifier.trainClassifier('contemplate solutions', 'nospam', oncomplete);
    classifier.trainClassifier('stupid question', 'nospam', oncomplete);
    test.done();
};

exports['classifytasks'] = function (test) {
    classifier.classifyContents('csharp programming', function (error, classification) {
        test.equal(classification, 'nospam');
        classifier.classifyContents('bitch stupid', function (error, classification) {
            test.equal(classification, 'spam');
            test.done();
        });
    });
};

