var fs = require('fs');
var inference = require('./../../routes/keywordinference.js').KeywordInference;
var POSTagger = require('./../../routes/postagger.js').POSTagger;
var Lexer = require('./../../routes/lexer.js').Lexer;

var utilities = require('./../../routes/utilities.js').Utility;

var lexer = new Lexer(function () {
});

var utility = new utilities();

var posTagger = new POSTagger();

exports['jqueryText'] = function (test) {
    var keywordInference = new inference();
    var text = keywordInference.filterJqueryText("<script></script><![CDATA[H sss jdkfdjlj sdljdslj dsdlsjdlsd jsldjsljd sdskjdljs dlsdjsl jdlsjdello] ]>");
    test.equal(text, "");
    test.done();
};

exports['splitSentences'] = function (test) {
    var keywordInference = new inference();
    var splitSentences = keywordInference.splitSentences("thisiesjsisj shsksh. shsudysjkksj sssd. sgsushshhhjhsjhjhjhjshdjshd. tsssss");
    test.equal(splitSentences.length, 3);
    test.done();
};

exports['scriptTags'] = function (test) {
    var keywordInference = new inference();
    var text = keywordInference.removeScript("<script>hey</script><li></li><!CDATA[ junk ]]><p>This is relevant.</p>");
    test.equal(text, "<li></li><!CDATA[ junk ]]><p>This is relevant.</p>");
    test.done();
};

exports['isValidText'] = function (test) {
    var keywordInference = new inference();
    var invalid = keywordInference.invalidText("-");
    test.equal(invalid, true);
    test.done();
};

exports['analyzeText'] = function (test) {
    var keywordInference = new inference();
    utility.enableTracing();

    fs.readFile('aloevera.html.txt', 'utf8', function (err, data) {
        if (err) {
            //TODO : logger print error
        }

        keywordInference.scrubHtml(data, lexer, posTagger, function (error, sentences) {
            keywordInference.analyzeText(sentences, function (sortedChiSquareValues) {
                var idx = 60;
                var currentIdx = 0;
                for (wordIdx in sortedChiSquareValues) {
                    if (currentIdx < idx && sortedChiSquareValues[wordIdx][0] == 'flower') {
                        test.done();
                        return;
                    }
                    currentIdx += 1;
                }
                test.done();
            });
        });
    });
};

exports['scrubHtml'] = function (test) {
    var keywordInference = new inference();

    keywordInference.scrubHtml("<script>hey</script><li></li><!CDATA[ junk ]]><p>This is relevant.</p>", lexer, posTagger, function (error, sentences) {
        test.equal(sentences.length, 1);
        test.equal(sentences[0].text, "This is relevant.");
        test.done();
    });
};
