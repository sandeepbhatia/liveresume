﻿var KMeansCluster = require('./../../routes/kmeansclustering.js').KMeansCluster;
var utilities = require('./../../routes/utilities.js').Utility;
var utility = new utilities();
var kMeansCluster = new KMeansCluster(utility);

var datapoints = [
    {
    	"data": [
            "access",
            "address",
            "allowing",
            "android",
            "application",
            "bookmark",
            "bookmarks",
            "browser",
            "button",
            "changes",
            "computer",
            "devices",
            "display",
            "don't",
            "don’t",
            "link",
            "menu",
            "name",
            "onlineandroidtips",
            "option",
            "options",
            "paste",
            "press",
            "setup",
            "shortcut",
            "site",
            "sites",
            "things",
            "url",
            "web",
            "webpage",
            "website",
            "window"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://www.onlineandroidtips.com/os/bookmarks.html",
    	"image": "http://www.onlineandroidtips.com/images/o13.jpg"
    },
    {
    	"data": [
            "(international",
            "(ô)",
            "ahd",
            "alphabet)",
            "boldface",
            "color:black;",
            "column",
            "dictionary",
            "examples",
            "forest",
            "four;",
            "hoarse;",
            "horse",
            "ipa",
            "letters",
            "list",
            "morning",
            "mourning",
            "pairs",
            "pronunciation",
            "pt;",
            "purposes",
            "rss",
            "sound",
            "symbols",
            "vowel",
            "were",
            "words"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://education.yahoo.com/reference/dictionary/pronunciation_key",
    	"image": "http://l.yimg.com/a/i/edu/reference/ereference/ereferencelogomed.gif"
    },
    {
    	"data": [
            "access",
            "address",
            "allowing",
            "android",
            "application",
            "bookmark",
            "bookmarks",
            "browser",
            "button",
            "changes",
            "computer",
            "devices",
            "display",
            "don't",
            "don’t",
            "link",
            "menu",
            "name",
            "onlineandroidtips",
            "option",
            "options",
            "paste",
            "press",
            "setup",
            "shortcut",
            "site",
            "sites",
            "things",
            "url",
            "web",
            "webpage",
            "website",
            "window"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://www.onlineandroidtips.com/os/bookmarks.html",
    	"image": "http://www.onlineandroidtips.com/images/o13.jpg"
    },
    {
    	"data": [
            "apollo",
            "bank",
            "bricks",
            "careercup",
            "change",
            "com",
            "company",
            "digital",
            "dover",
            "dow",
            "ericsson",
            "facebook",
            "gaming",
            "globaltech",
            "gluster",
            "gnwebsoft",
            "godaddy",
            "goldman",
            "gowdanar",
            "graduate",
            "green",
            "group",
            "groupon",
            "grubhub",
            "guruji",
            "harman",
            "headrun",
            "health",
            "hewlett",
            "inc",
            "india",
            "infotech",
            "int",
            "kadron",
            "ltd",
            "networks",
            "number",
            "one",
            "packard",
            "pvt",
            "research",
            "return",
            "sachs",
            "software",
            "student",
            "technologies",
            "technology",
            "value",
            "wap",
            "wireless"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://www.careercup.com/page",
    	"image": "http://www.careercup.com/images/bookstars.jpg"
    },
    {
    	"data": [
            "actions",
            "apple",
            "apple’s",
            "apps",
            "are",
            "aspect",
            "befitting",
            "bitcoin",
            "business",
            "companies",
            "conduct",
            "control",
            "course",
            "drug",
            "dupe",
            "example",
            "facebook",
            "gadgets",
            "game",
            "games",
            "gamification",
            "hand",
            "ideals",
            "industry",
            "ios",
            "it’s",
            "life",
            "maintaining",
            "makers",
            "manner",
            "market",
            "medium",
            "platform",
            "plenty",
            "profit",
            "regulation",
            "shadows",
            "society",
            "sort",
            "techcrunch",
            "thinking",
            "today",
            "use",
            "user",
            "was",
            "work",
            "writers"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://techcrunch.com/2013/04/13/what-games-are-the-shady-side-of-games/",
    	"image": "http://tctechcrunch2011.files.wordpress.com/2013/04/800px-three_card_monte.jpg?w=300"
    },
    {
    	"data": [
            "(this",
            "[endif]",
            "android",
            "article",
            "bit",
            "board",
            "bookmark",
            "bookmarks",
            "box",
            "browser",
            "button",
            "code",
            "comment",
            "day",
            "finger",
            "image",
            "images",
            "important)",
            "it’",
            "java",
            "list",
            "logo",
            "marketing",
            "menu",
            "month",
            "page",
            "phone",
            "pin",
            "pinterest",
            "post",
            "posted",
            "script",
            "selection",
            "something",
            "staying",
            "this:",
            "use",
            "was",
            "website",
            "we’ve",
            "works",
            "‘address’",
            "‘paste’",
            "‘pin"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://truesilver.co.uk/2012/04/30/pinterest-pin-it-button-for-android/",
    	"image": "http://truesilver.co.uk/wp-content/themes/truesilver2013/slider/slider3.png"
    },
    {
    	"data": [
            "(networking)",
            "000",
            "address",
            "aol",
            "app",
            "band",
            "becoming",
            "bitcoin",
            "chance",
            "colors",
            "column",
            "consumer",
            "day",
            "ended",
            "facebook",
            "fashion",
            "fitness",
            "gadgets",
            "gate",
            "home",
            "host",
            "leds",
            "life",
            "linkme",
            "met",
            "new",
            "notification",
            "platform",
            "products",
            "rainbow",
            "relaunch",
            "research",
            "reticle",
            "seconds",
            "services",
            "show",
            "smartphones",
            "team",
            "tech",
            "techcrunch",
            "technology",
            "techspressive",
            "there’s",
            "three",
            "time",
            "vowed",
            "web",
            "weloxx",
            "whacked:",
            "wrists"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://techcrunch.com/2013/04/13/backed-or-whacked-join-together-with-the-band/",
    	"image": "http://tctechcrunch2011.files.wordpress.com/2013/04/bw-weloxx.jpg?w=300"
    },
    {
    	"data": [
            "advertising",
            "apps",
            "beachmint",
            "bigs",
            "bitcoin",
            "boutique",
            "brand",
            "brands",
            "campaigns",
            "ceo",
            "companies",
            "data",
            "effort",
            "fab",
            "facebook",
            "flagship",
            "gadgets",
            "gilt",
            "glasses",
            "google",
            "groupe",
            "home",
            "hoped",
            "it’s",
            "kings",
            "lane",
            "lenscrafters",
            "luxottica",
            "market",
            "months",
            "new",
            "nyc",
            "offline",
            "one",
            "others",
            "parker",
            "percent",
            "showrooms",
            "sites",
            "six",
            "startup",
            "store",
            "sunglass",
            "techcrunch",
            "time)",
            "viewers",
            "warby",
            "was",
            "wayfair",
            "york"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://techcrunch.com/2013/04/13/warby-parker-opens-retail-store-in-nyc-with-boston-up-next-beats-google-amazon-to-the-offline-punch/",
    	"image": "http://tctechcrunch2011.files.wordpress.com/2013/04/tumblr_inline_ml5drgq2sc1qz4rgp.jpg?w=500"
    },
    {
    	"data": [
            "000",
            "ball",
            "bitcoin",
            "body",
            "com",
            "crowdfunding",
            "email",
            "end",
            "eof",
            "facebook",
            "gadgets",
            "gang:",
            "gillmor",
            "glass:",
            "got",
            "hiring",
            "home",
            "integration",
            "it’s",
            "livefyre",
            "looking",
            "motor",
            "music",
            "new",
            "offer",
            "parts",
            "product",
            "range",
            "resonating",
            "sales people",
            "seconds",
            "sensations",
            "speculation",
            "speeds",
            "story",
            "strain",
            "tags:",
            "techcrunch",
            "tennis",
            "tip",
            "vibration",
            "vibrator",
            "vibrators",
            " death"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://techcrunch.com/2013/04/13/revel-body-is-a-crowdfunded-personal-massager/",
    	"image": "https://s3.amazonaws.com/ksr/projects/29651/photo-main.jpg?1302639516"
    },
    {
    	"data": [
            "ago):",
            "allowing",
            "billion",
            "bitcoin",
            "bitcoin’s",
            "came",
            "ceased",
            "commerce",
            "credits",
            "currency",
            "currency;",
            "deal",
            "don’t",
            "everyone’s",
            "facebook",
            "felix",
            "first",
            "gadgets",
            "having",
            "home",
            "hype",
            "ignorance;",
            "instance",
            "isn’t",
            "it’s",
            "market",
            "mines",
            "month",
            "payment",
            "payments",
            "post",
            "racks",
            "right",
            "said:",
            "sign",
            "smiled",
            "techcrunch",
            "that’s",
            "thing",
            "thinking",
            "threat",
            "was",
            "way",
            "weeks",
            "world",
            "year",
            "you’re"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://techcrunch.com/2013/04/13/beyond-the-bitcoin-bubble/",
    	"image": "http://i1.ytimg.com/vi/Tv5gBFqzQfY/hqdefault.jpg"
    },
    {
    	"data": [
            "(in",
            "ago):",
            "behemoth",
            "billion",
            "bitcoin",
            "bitcoin’s",
            "came",
            "ceased",
            "commerce",
            "credits",
            "currency",
            "currency;",
            "deal",
            "don’t",
            "everyone’s",
            "facebook",
            "first",
            "gadgets",
            "home",
            "hype",
            "ignorance;",
            "instance",
            "it’s",
            "market",
            "mines",
            "month",
            "payment",
            "payments",
            "post",
            "racks",
            "reason",
            "right",
            "said:",
            "salmon",
            "smiled",
            "techcrunch",
            "that’s",
            "thing",
            "thinking",
            "threat",
            "was",
            "way",
            "weeks",
            "world",
            "wrote",
            "year",
            "you’re"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://techcrunch.com/2013/04/13/beyond-the-bitcoin-bubble/",
    	"image": "http://i1.ytimg.com/vi/Tv5gBFqzQfY/hqdefault.jpg"
    },
    {
    	"data": [
            "(we",
            "api",
            "began",
            "behance",
            "bookmarklet",
            "both",
            "cases",
            "connection",
            "content",
            "course",
            "developer",
            "developers",
            "documentation",
            "don’t",
            "extension",
            "favourite",
            "feature",
            "gallery",
            "github",
            "improvements",
            "it’s",
            "kippt",
            "link",
            "links",
            "list",
            "lists",
            "mix:",
            "nokia’s",
            "one",
            "party",
            "people",
            "posting",
            "posts",
            "share",
            "support",
            "tags",
            "team",
            "things",
            "use",
            "user",
            "users",
            "videos",
            "visit",
            "way",
            "we’re",
            "work",
            "year",
            "you’re"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://blog.kippt.com/",
    	"image": "http://blog.kippt.com/images/img-tile.jpg"
    },
    {
    	"data": [
            "[endif]",
            "accompli",
            "algorithm",
            "algorithms",
            "approaches",
            "award",
            "book",
            "classes",
            "courses",
            "design",
            "designing",
            "discussion",
            "doubled",
            "edition",
            "engineering",
            "exam",
            "exercises",
            "fait",
            "false",
            "features",
            "first",
            "homework",
            "ideas",
            "ieee",
            "implementations",
            "include:",
            "line",
            "material",
            "obscuring",
            "one",
            "part",
            "problem",
            "problems",
            "reasons",
            "received",
            "science",
            "second",
            "students",
            "teaching",
            "textbooks",
            "tutorial",
            "twenty",
            "was",
            "years"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://www.algorist.com/",
    	"image": "http://www.algorist.com/cover-old.gif"
    },
    {
    	"data": [
            "000",
            "activities",
            "articles",
            "authors",
            "backstory",
            "blog",
            "books",
            "boston",
            "candlepower",
            "classroom",
            "click",
            "column",
            "count",
            "creatives",
            "dictionary",
            "dog",
            "euphemisms",
            "excerpts",
            "fun",
            "language",
            "lexicographers",
            "lexicon",
            "lounge",
            "maneuvers",
            "map",
            "marketing",
            "page",
            "questions",
            "sign",
            "spelling",
            "talk",
            "teaching",
            "thesaurus",
            "topics:",
            "using",
            "vocab",
            "vocabulary",
            "wake",
            "word",
            "words",
            "words(",
            "words)",
            "wordshop",
            "work",
            "writers",
            "writing"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://www.visualthesaurus.com/",
    	"image": "http://www.visualthesaurus.com/browse/default/en/search.gif"
    },
    {
    	"data": [
            "(address",
            "(required)",
            "added",
            "august",
            "background",
            "check",
            "click",
            "color",
            "colors",
            "commenting",
            "default",
            "details",
            "effect",
            "email",
            "end",
            "gvim",
            "icon",
            "light",
            "log",
            "name",
            "note",
            "note:",
            "post",
            "posts",
            "property",
            "public)",
            "scheme",
            "setting",
            "settings",
            "switch",
            "switching",
            "terminal",
            "theme",
            "use",
            "using",
            "vim",
            "vimrc",
            "website",
            "wordpress"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://choorucode.com/2011/07/29/vim-use-background-to-match-colors-of-windows-console/",
    	"image": "http://choorucode.files.wordpress.com/2011/07/20110729-vim-background.png?w=696"
    },
    {
    	"data": [
            "added",
            "addition",
            "asked",
            "assuming",
            "bst",
            "candidates",
            "careercup",
            "company",
            "ctci",
            "data",
            "depending",
            "end",
            "facebook",
            "finding",
            "function",
            "heap",
            "interview",
            "interviews",
            "link",
            "list",
            "location",
            "max",
            "memory",
            "numbers",
            "page",
            "questions",
            "reference",
            "result",
            "review",
            "right",
            "right)",
            "root",
            "said",
            "store",
            "time",
            "trade",
            "tree",
            "twitter",
            "use",
            "value",
            "videos",
            "videoscareercup's",
            "word"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://www.careercup.com/",
    	"image": "http://1-ps.googleusercontent.com/x/s.careercup-hrd.appspot.com/www.careercup.com/images/xslide0.jpg.pagespeed.ic.AFpv0Z4IU_.png"
    },
    {
    	"data": [
            "(and",
            "accelerator",
            "amazon",
            "apple",
            "bitcoin",
            "books",
            "building",
            "building’s",
            "by:",
            "chris",
            "company",
            "dannen",
            "data",
            "design",
            "experience",
            "facebook",
            "foursquare's",
            "friction",
            "game",
            "gap:",
            "goodreads",
            "head",
            "here’s",
            "human)",
            "hurdles",
            "minding",
            "open",
            "people",
            "philadelphia",
            "pong",
            "pong:",
            "read",
            "researchers",
            "search",
            "side",
            "software",
            "story",
            "today’s",
            "tracking",
            "user",
            "users",
            "we're",
            "women",
            "world",
            "world’s",
            "writ",
            "years"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://www.fastcolabs.com/",
    	"image": "http://www.fastcolabs.com/multisite_files/fastcompany/imagecache/1280/poster/2013/04/3008596-poster-1920-amazon-snaps-goodreads.jpg"
    },
    {
    	"data": [
            "api",
            "app",
            "card",
            "considering",
            "emerson",
            "everyone",
            "facebook",
            "first",
            "found",
            "friends",
            "gupta",
            "guy",
            "hackathon",
            "hockey",
            "home",
            "hours",
            "information",
            "it’s",
            "location",
            "london",
            "market",
            "michael",
            "morris",
            "one",
            "order",
            "others",
            "peek",
            "people",
            "perret and",
            "place",
            "project",
            "purchases",
            "seconds",
            "splayed",
            "techcrunch",
            "thack",
            "they’ve",
            "time",
            "took",
            "travel",
            "trying",
            "users",
            "vishal",
            "way",
            "winter",
            "working",
            "zach"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://techcrunch.com/2013/04/27/meet-the-hackers-and-their-promising-projects-at-the-disrupt-ny-hackathon/",
    	"image": "http://tctechcrunch2011.files.wordpress.com/2013/04/hackcrowd13.jpg?w=640"
    },
    {
    	"data": [
            "access",
            "acquisition",
            "ads",
            "android",
            "app",
            "apps",
            "backend",
            "benefit",
            "business",
            "came",
            "changes",
            "com",
            "complaints",
            "data",
            "developers",
            "entry",
            "facebook",
            "facebook’s",
            "integrations",
            "i’ve",
            "network",
            "parse",
            "parse’s",
            "platform",
            "policy",
            "privacy",
            "product",
            "pushing",
            "questions",
            "record",
            "records",
            "responsibilities",
            "rights",
            "service",
            "services",
            "stackmob",
            "terms",
            "tool",
            "use",
            "using",
            "was",
            "worries"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://techcrunch.com/2013/04/27/parse-facebook/",
    	"image": "http://tctechcrunch2011.files.wordpress.com/2013/04/parse-features.png?w=640"
    },
    {
    	"data": [
            "000",
            "aol",
            "api",
            "appery",
            "bizspark",
            "cash",
            "column",
            "crook",
            "crunchbase",
            "fire",
            "flame",
            "fuel",
            "gadgets",
            "general",
            "got",
            "greentech",
            "guest",
            "hackathon",
            "hackers",
            "integration",
            "livefyre",
            "microsoft",
            "monday",
            "pearson",
            "prizes",
            "production",
            "right",
            "samsung",
            "season",
            "seconds",
            "skydrive",
            "sponsors",
            "story",
            "tech",
            "techcrunch",
            "tip",
            "topic",
            "twilio",
            "visa",
            "we've",
            "with:",
            "working",
            "wrigley",
            "yammer",
            "you've"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://techcrunch.com/2013/04/27/the-techcrunch-disrupt-ny-hackathon-is-on-and-poppin/",
    	"image": "https://tctechcrunch2011.files.wordpress.com/2013/03/ny13-tickets4.jpg?w=640"
    },
    {
    	"data": [
            "(which",
            "accounts",
            "acknowledgement",
            "android",
            "app",
            "apple",
            "apple’s",
            "apps",
            "arc",
            "bunch",
            "care",
            "companies",
            "company",
            "couple",
            "day",
            "department",
            "details",
            "devices",
            "doesn’t",
            "dozen",
            "efforts",
            "end",
            "facebook",
            "google",
            "insane)",
            "instagram",
            "iphone",
            "iphones",
            "it’s",
            "kind",
            "lot",
            "one",
            "people",
            "pictures",
            "piece",
            "pitching",
            "platform",
            "something",
            "statistic",
            "that’s",
            "times",
            "use",
            "wasn’t",
            "way"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://thenextweb.com/apple/2013/04/27/apples-new-iphone-ad-instagram-and-platform-sublimation/?fromcat=all",
    	"image": "http://cdn.thenextweb.com/wp-content/blogs.dir/1/files/2013/04/Screen-Shot-2013-04-26-at-4.12.52-PM-645x250.jpg"
    },
    {
    	"data": [
            "(which",
            "accounts",
            "acknowledgement",
            "android",
            "app",
            "apple",
            "apple’s",
            "apps",
            "arc",
            "bunch",
            "care",
            "companies",
            "company",
            "couple",
            "day",
            "department",
            "details",
            "devices",
            "doesn’t",
            "dozen",
            "efforts",
            "end",
            "facebook",
            "google",
            "insane)",
            "instagram",
            "iphone",
            "iphones",
            "it’s",
            "kind",
            "lot",
            "one",
            "people",
            "pictures",
            "piece",
            "pitching",
            "platform",
            "something",
            "statistic",
            "that’s",
            "times",
            "use",
            "wasn’t",
            "way"
    	],
    	"clusterId": -1,
    	"association": -1,
    	"clusterDistance": -1,
    	"articleUrl": "http://thenextweb.com/apple/2013/04/27/apples-new-iphone-ad-instagram-and-platform-sublimation/?fromcat=all",
    	"image": "http://cdn.thenextweb.com/wp-content/blogs.dir/1/files/2013/04/Screen-Shot-2013-04-26-at-4.12.52-PM-645x250.jpg"
    }
];
exports['getclusters'] = function (test) {
	kMeansCluster.ComputeClusters(3, datapoints, function (datapoints) {
		var zero= 0;
		var one = 0;
		var two = 0;

		for (var point in datapoints) {
			utility.trace(datapoints[point].articleUrl);
			utility.trace(datapoints[point].association);
			if (datapoints[point].association == 0) {
				zero++;
			}
			if (datapoints[point].association == 1) {
				one++;
			}
			if (datapoints[point].association == 2) {
				two++;
			}
		}
		test.done();
	});
};
