var dataGatherer = require('./../../routes/datagatherer.js').DataGatherer;

exports['connection'] = function (test) {
    var data = new dataGatherer('localhost', 27017, function (obj) {
        test.notEqual(obj, undefined);
        test.notEqual(obj, null);
    });
    test.done();
};

exports['getskillcollection'] = function (test) {
    var data = new dataGatherer('localhost', 27017, function (obj) { });

    data.findAll(function (error, skillArtifactCollection) {
        test.notEqual(skillArtifactCollection, undefined);
        test.notEqual(skillArtifactCollection, null);
    });
    test.done();
};


exports['saveSkill'] = function (test) {
    var data = new dataGatherer('localhost', 27017, function (obj) { });

    data.SaveSkillArtifact({ title: 'UNITTEST__TITLE',
        data: 'UNITTEST__DATA',
        url: 'http://www.liveresume.com',
        data: 'UNITEST_DATA',
        chi: ['unittest', 'test']
    },
				function (error, skillArtifact) {
				    test.notEqual(skillArtifact, undefined);
				    test.notEqual(skillArtifact, null);
				});
    test.done();
};

exports['search'] = function (test) {
    var data = new dataGatherer('localhost', 27017, function (obj) { });


    data.SaveSkillArtifact({ title: 'UNITTEST__TITLE',
        data: 'UNITTEST__DATA',
        url: 'http://www.liveresume.com',
        data: 'UNITEST_DATA',
        chi: ['unittest', 'test']
    },
				function (error, skillArtifact) {
				    test.notEqual(skillArtifact, undefined);
				    test.notEqual(skillArtifact, null);

				    data.findAll(function (error, skillArtifactCollection) {
				        test.done();
				    }, 'unitest');
				});
    test.done();
};

